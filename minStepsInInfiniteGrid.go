package main

import (
	"math"
)

/**
 * @input A : Integer array
 * @input B : Integer array
 *
 * @Output Integer
 */
func coverPoints(A []int, B []int) int {
	var result int

	for i := 1; i < len(A); i++ {
		result += int(math.Max(math.Abs(float64(B[i]-B[i-1])), math.Abs(float64(A[i]-A[i-1]))))
	}

	return result
}
