package main

import (
	"sort"
	"strconv"
)

/**
 * @input A : array of strings
 *
 * @Output Integer
 */
func solve(A []string) int {
	var numbers = make([]float64, len(A))

	for i, a := range A {
		numb, _ := strconv.ParseFloat(a, 64)
		numbers[i] = numb
	}

	sort.Slice(numbers, func(i, j int) bool {
		return numbers[i] < numbers[j]
	})

	var left = 0
	var right = len(A) - 1
	var middle = left + 1

	for left < right-1 {
		middle = left + 1

		for middle < right && numbers[left]+numbers[middle]+numbers[right] <= 1.0 {
			middle++
		}

		if middle == right {
			left++
			continue
		}

		for middle > left && numbers[left]+numbers[middle]+numbers[right] >= 2.0 {
			middle--
		}

		if middle == left {
			right--
			continue
		}

		return 1
	}

	return 0
}
