package main

import "sort"

/**
 * @input A : Integer array
 *
 * @Output Integer
 */
func solve(A []int )  (int) {

	apparitions := make(map[int]int)

	for i := len(A) - 1; i >= 0; i-- {
		apparitions[A[i]] = apparitions[A[i]] + 1
	}

	greaterValuesCount := 0
	keys := make([]int, 0, len(apparitions))
	for k := range apparitions {
		keys = append(keys, k)
	}

	sort.Slice(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})

	for i := len(keys) - 1; i >= 0; i-- {
		value := apparitions[keys[i]]
		if keys[i] == greaterValuesCount {
			return 1
		}
		greaterValuesCount += value
	}

	return -1
}
