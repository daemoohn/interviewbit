package main

import "sort"

/**
 * @input A : Integer array
 *
 * @Output Integer array.
 */
func wave(A []int )  ([]int) {
	var result = make([]int, len(A))
	copy(result, A)

	sort.Slice(result, func(i, j int) bool {
		return result[i]<result[j]
	})

	for i:= 0; i< len(result); i+=2{
		if i + 1 <len(result){
			var tmp = result[i]
			result[i] = result[i+1]
			result[i+1]=tmp
		}
	}

	return result
}
