package main

/**
 * @input A : String
 *
 * @Output Integer array.
 */
func flip(A string) []int {

	var resultLeft = -1
	var resultRight = -1

	var left = -1
	var right = -1
	var maxCost = -1
	var cost = -1

	for idx, a := range A {
		if a == '0' {

			if cost < 0 {
				cost = 0
				left = idx
			}
			cost++
			right = idx

			if maxCost < cost {
				maxCost = cost
				resultLeft = left
				resultRight = right
			}
		} else {
			cost--
		}
	}

	if maxCost > 0 {
		return []int{resultLeft + 1, resultRight + 1}
	}

	return []int{}
}
