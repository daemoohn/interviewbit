package main

import "sort"

/**
 * @input A : Integer array
 *
 * @Output Integer
 */
func maximumGap(A []int )  (int) {

	var items = make([]Item, len(A))

	for idx, a := range A {
		items[idx] = Item{ Index:idx, Value: a}
	}

	sort.Slice(items, func(i, j int) bool {
		return items[i].Value < items[j].Value
	})

	var min = items[0].Index
	var max = items[0].Index
	var dist = max - min

	for i := 1; i<len(A); i++ {
		if items[i].Index < min {
			min = items[i].Index
			max = items[i].Index

			if max - min > dist {
				dist = max - min
			}
		}
		if items[i].Index > max {
			max = items[i].Index

			if max - min > dist {
				dist = max - min
			}
		}
	}

	return dist
}

type Item struct {
	Index int
	Value int
}
