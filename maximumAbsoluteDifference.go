package main

import "math"

/**
 * @input A : Integer array
 *
 * @Output Integer
 */
func maxArr(A []int) int {

	var maxDif = math.MinInt32
	var minDif = math.MaxInt32
	var maxSum = math.MinInt32
	var minSum = math.MaxInt32

	for i := 0; i < len(A); i++ {

		var sum = A[i] + i
		var dif = A[i] - i

		maxSum = int(math.Max(float64(maxSum), float64(sum)))
		maxDif = int(math.Max(float64(maxDif), float64(dif)))

		minSum = int(math.Min(float64(minSum), float64(sum)))
		minDif = int(math.Min(float64(minDif), float64(dif)))

	}

	return int(math.Max(float64(maxSum - minSum), float64(maxDif - minDif)))
}
