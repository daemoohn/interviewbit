package main

/**
 * @input A : Integer array
 *
 * @Output Integer
 */
func maxSubArray(A []int) int {
	var largestSum = A[0]
	var runningSum = A[0]

	for i := 1; i < len(A); i++ {

		if runningSum < 0 {
			runningSum = A[i]
		} else {
			runningSum += A[i]
		}

		if runningSum > largestSum {
			largestSum = runningSum
		}
	}

	return largestSum
}