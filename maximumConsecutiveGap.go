package main

import "math"

/**
 * @input A : Integer array
 *
 * @Output Integer
 */
func maximumGap1(A []int) (int) {
	if len(A) < 2 {
		return 0
	}

	var min = math.MaxInt32
	var max = math.MinInt32

	for _, a := range A {
		if min > a {
			min = a
		}
		if max < a {
			max = a
		}
	}

	if min == max {
		return 0
	}

	var gap = (max - min) / (len(A) - 1)
	if gap == 0 {
		gap = 1
	}
	//1, 10, 5=>gap = 4
	//min->min + gap -1;min + gap -> min +2*gap-1

	var bucketsCount = (max - min + 1) / gap
	if (max-min+1)%gap != 0 {
		bucketsCount++
	}

	var buckets = make([]GapInterval, bucketsCount)
	for i := 0; i < bucketsCount; i ++ {
		buckets[i] = GapInterval{MinNominal: min + i*gap, MaxNominal: min + (i+1)*gap - 1, MinActual: -1, MaxActual: -1}
	}

	for _, a := range A {
		var bucketIdx = (a - min) / gap
		if buckets[bucketIdx].MinActual == -1 || buckets[bucketIdx].MinActual > a {
			buckets[bucketIdx].MinActual = a
		}
		if buckets[bucketIdx].MaxActual == -1 || buckets[bucketIdx].MaxActual < a {
			buckets[bucketIdx].MaxActual = a
		}
	}

	var tempMin = 0
	var tempMax = 0

	var delta = 0
	for i := 0; i < bucketsCount-1; i++ {
		if buckets[i].MaxActual != -1 {
			tempMax = buckets[i].MaxActual
		}
		if buckets[i+1].MinActual != -1 {
			tempMin = buckets[i+1].MinActual
		}
		if tempMin-tempMax > delta {
			delta = tempMin - tempMax
		}
	}

	if gap > delta {
		return gap
	}

	return delta
}

type GapInterval struct {
	MinNominal int
	MaxNominal int
	MinActual  int
	MaxActual  int
}