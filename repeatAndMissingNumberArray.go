package main

/**
 * @input A : Integer array
 *
 * @Output Integer array.
 */
func repeatedNumber(A []int) []int {
	var actualSum int64
	var actualSumSquares int64

	var n = int64(len(A))
	var sum = n * (n + 1) / 2
	var sumSquares = n * (n + 1) * (2*n + 1) / 6

	for _, a := range A {
		actualSum += int64(a)
		actualSumSquares += int64(a) * int64(a)
	}

	return []int{int((sumSquares-actualSumSquares)/(sum-actualSum) - ((sum-actualSum)+(sumSquares-actualSumSquares)/(sum-actualSum))/2),
		int(((sum - actualSum) + (sumSquares-actualSumSquares)/(sum-actualSum)) / 2)}
}
