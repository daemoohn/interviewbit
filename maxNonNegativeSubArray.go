package main

/**
 * @input A : Integer array
 *
 * @Output Integer array.
 */
func maxset(A []int) []int {
	var maxSum = -1
	var maxLeft = -1
	var maxRight = -1

	var sum int
	var left = -1
	var right = -1

	for idx, a := range A {
		if a >= 0 {
			if left < 0 {
				left = idx
				sum = 0
			}
			right = idx
			sum += a

			if maxSum < sum {
				maxSum = sum
				maxLeft = left
				maxRight = right
			} else {
				if maxSum == sum {
					if maxRight-maxLeft < right-left {
						maxRight = right
						maxLeft = left
					} else {
						if maxRight-maxLeft == right-left {
							if maxRight > right {
								maxRight = right
								maxLeft = left
							}
						}
					}
				}
			}
		} else {
			left = -1
			right = -1
		}
	}

	if maxSum >= 0 {
		return A[maxLeft : maxRight+1]
	}
	return []int{}

}
