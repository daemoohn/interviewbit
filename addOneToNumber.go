package main

func plusOne(A []int) []int {
	var result = A

	var length = len(result)

	var carry = 1
	for i := length - 1; i >= 0; i-- {
		result[i] += carry
		if result[i] == 10 {
			result[i] = 0
			carry = 1
		} else {
			carry = 0
		}
	}

	if carry == 1 {
		result = append([]int{carry}, result...)
	}

	var i int

	for i = 0; i < len(result); i++ {
		if result[i] != 0 {
			break
		}
	}
	result = result[i:]
	return result
}