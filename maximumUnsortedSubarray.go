package main

import (
	"sort"
)

/**
 * @input A : Integer array
 *
 * @Output Integer array.
 */
// 1, 2, 3, 5, 6, 13, 15, 16, 17, 13, 13, 15, 17, 17, 17, 17, 17, 19, 19
func subUnsort(A []int) []int {

	var sortedA = make([]int, len(A))
	copy(sortedA, A)
	sort.Slice(sortedA, func(i, j int) bool {
		return sortedA[i] < sortedA[j]
	})

	var left = -1
	var right = -1

	for i := 1; i < len(A); i++ {
		if sortedA[i] != A[i] {
			if left == -1 {
				left = i
			} else {
				right = i
			}
		}
	}

	if left == -1 {
		return []int{-1}
	}

	return []int{left, right}
}
